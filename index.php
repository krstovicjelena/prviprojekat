<?php
	header('Content-type: application/json; charset=utf-8');

	echo json_encode([
		'status' => -100001,
		'time' => microtime(true),
		'message' => 'Illegal API call',
		'data' => (object) [],
	]);
